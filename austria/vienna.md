# Vienna

Capital of Austria, [Map](https://goo.gl/maps/QWD2vHGRMXdMr3vW8).

Learn to speak Viennese in one word: https://www.youtube.com/watch?v=iuXR53ex4iI 

## Organisation

- Public transport, get a 24, 48 or 72 hours ticket from Wiener Linien, https://www.wienerlinien.at/eportal3/ep/channelView.do/pageTypeId/66533/channelId/-47345
- When arriving by plane, either take the CAT train, or a taxi (usally at 35-40€ til the inner city).
- When arriving by train, the central station is located south of Vienna. The near underground station takes you anywhere to the center.

Make sure to either get an app, or a printed plan for the underground stations. `Stephansplatz` is the center of Vienna and a good orientation spot. 
https://www.wienerlinien.at/eportal3/ep/channelView.do/pageTypeId/66526/channelId/-3600060

## Sightseeing

If you are not into guided bus tours, you really can explore the city on foot and just hop on/off the underground and trams. Also, better trust the official pages than TripAdvisor ;-)

https://www.wien.info/en/sightseeing/sights 

You can connect the inner city sightseeing spots just with walking by the [Ringstrasse](https://www.wien.info/en/sightseeing/architecture-design/ringstrasse).


- [St. Stephen's Cathedral](https://www.wien.info/en/sightseeing/sights/st-stephens-cathedral), "Stephansdom". The heart of Vienna, history in all its beauty. 
- [MuseumsQuartier](https://www.wien.info/en/sightseeing/museums-exhibitions/top/mq). Unique art in its own way. It is connected to the Ringstrasse, and opens the Mariahilferstrasse with its many coffee spots to walk by.
- [Belvedere](https://www.wien.info/en/sightseeing/museums-exhibitions/top/belvedere) is a beauty of its own.
- [Votiv Church](https://www.wien.info/en/sightseeing/sights/from-s-to-z/votivkirche). Walk by and get to know one of Vienna's monuments.
- [Secession](https://www.wien.info/en/sightseeing/sights/art-nouveau/secession). Located near Naschmarkt, worth a view on the golden dome.
- [Spanish Riding School](https://www.wien.info/en/sightseeing/sights/imperial/spanish-riding-school). 
- [Hundertwasser House](https://www.wien.info/en/sightseeing/sights/hundertwasser-house-vienna). Well known architecture.
- [University of Vienna](https://www.wien.info/en/sightseeing/sights/university-of-vienna). The largest university in Austria, and maybe you'll spot my former workplace ;-)

### A view over Vienna

- [Giant Ferris Wheel](https://www.wien.info/en/sightseeing/prater/giant-ferris-wheel), "Riesenrad". Plan for a sunny day, the view over the city is unbelievable.
    - Stay for a while and visit the [Prater](https://www.wien.info/en/sightseeing/prater).
- [Kahlenberg](https://www.wien.info/en/sightseeing/green-vienna/Kahlenberg). Take a walk, or go by the bus to get a different view over Vienna. _This is an insider tip._
    - Höhenstraße, https://vienna-insight.at/blog/2018/10/16/havent-seen-vienna-unless-youve-kahlenberg/ 
- [Danube Tower](https://www.wien.info/en/sightseeing/sights/from-a-to-z/danube-tower). A panoramic view over Vienna.

### On a sunny day

- [Fiaker Horse-drawn carriage](https://www.wien.info/en/sightseeing/fiaker-horse-drawn-carriage). Not cheap, but one for the bucket list. Better than a bus tour.
- [Schönbrunn Palace](https://www.wien.info/en/sightseeing/sights/imperial/schoenbrunn-palace). Enjoy architecture and nature. Make sure to visit the zoo as well!

### On a rainy day

Museums invite to explore.

- [House of the Sea](https://www.wien.info/en/vienna-for/families/indoor/sea-house). Imho a must visit.
- [Natural History Museum](https://www.wien.info/en/sightseeing/museums-exhibitions/top/museum-natural-history)
- [Museum of Technology](https://www.wien.info/en/sightseeing/sights/from-s-to-z/technical-museum)



## Food and Beverages

- Take a walk on the Mariahilferstrasse, this connects Westbahnhof with Museumsquartier/Volksgarten. Combine shopping with getting coffee and pastry confections. The house of the sea also is located there.
    - Freiraum Café: https://goo.gl/maps/A8FLJiqS848RTnYE8
- [Vienna Coffeehouses](https://www.wien.info/en/shopping-wining-dining/coffeehouses), follow the tradition. 
- Take a walk on the [Naschmarkt](https://www.wien.info/en/shopping-wining-dining/markets/naschmarkt) and join the local market and the traditional coffeehouses.

Personal recommendation for restaurants:

- Brandauer Bierbögen: https://goo.gl/maps/TzUBYA94q3pWrnb28
- Fischer Bräu: https://goo.gl/maps/YMTqBtNaFPtGDv3Q8
- Siebensternbräu: https://goo.gl/maps/rxAA6NB9NbN1C5ck7


### Specialities

#### Cakes

- [Sacher cake](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/sachertorte), "Sachertorte". Best to be enjoyed with a good Melange. 
- [Apple Strudel](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/apple-strudel), Apfelstrudel. Best with vanilla sauce/custard.     
- [Cardinal slices](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/cardinal-slices), Kardinalschnitte. Fresh and fluffy.
- [Esterházytorte](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/esterhazytorte). Unique in its taste.
- [Malakoff-Schokolade-Torte (Malakov Chocolate Torte)](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/malacov-chocolate-torte). Feels like in heaven. Don't count the calories, just enjoy.
- [Marmorgugelhupf (Marble Bundt Cake)](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/marmorgugelhupf). So fluffy. 


#### Warm Sweets

- [Topfenstrudel (Cream cheese strudel)](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/cream-cheese-strudel)
- [Germknödel (Yeast dumplings)](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/yeast-dumplings)
- [Topfenknödel (Curd cheese dumplings)](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/topfenknoedel). Filled with jam, fresh fruits, pure, nougat, whatever you like.
- [Kaiserschmarren](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/kaiserschmarren)
- [Mohnnudeln (Poppy seed noodles)](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/poppy-seed-noodles). My grandma used to make them, always delicious childhood memories. 
- [Buchteln mit Vanillesauce (Baked Yeast Buns with Vanilla Sauce/Custard)](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/buchteln-a-la-sacher)
- [Topfenpalatschinken (Curd Cheese Pancakes)](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/cheese-crepes)
    - A more specific variant is with jam, apricot or cranberry. This is what you get in most local restaurants. 
    - The name "Palatschinke" originates from Hungary.

#### Main dishes

- Wiener Schnitzel (calf, not pork)
    - Some say, Figlmüller is the best. On the other hand, visiting not so famous local restaurants also is a good idea.
    - https://www.1000things.at/blog/die-besten-wiener-schnitzel-teil-1/ 
- Tafelspitz
    - Plachutta: https://www.plachutta-wollzeile.at/en 
- [Wiener Saftgulasch (Viennese goulash)](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/viennese-goulash)
- [Frittaten (crêpe slivers)](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/frittaten)
- [Kalbsleberknödel (Veal liver dumplings)](https://www.wien.info/en/shopping-wining-dining/viennese-cuisine/recipes/veal-liver-dumplings)

### Cafés 

- Café Demel (pricy, for the bucket list): https://goo.gl/maps/aGt6eoz9Gb7Nj75k6
- Café Sacher (pricy, bucket list): https://goo.gl/maps/qaVRQ3XE2VUQXjMs5
- Café Central: https://goo.gl/maps/WgbTyiYuVUGzGmqc7
- Café Landtmann: https://goo.gl/maps/TqvzfEdeSz8DdwNd6 
- Aida Café Konditorei, the biggest franchise café in Vienna

There is a variety of different coffees available. Try them all and put them on the bucket list. 
Can you explain the difference between a capucchino and a melange? 

### This and that

- Käsekrainer, 
    - There is a local saying: [A Eitrige mit an Krokodü, an G'schissenen und an 16er Blech](https://www.vice.com/de/article/jpan43/a-eitrige-mit-an-krokodu-gschissenem-und-an-16er-blech-124)
        - Eitrige: Sausage with cheese inside
        - G'schissenen: Mustard, yellow
        - 16er Blech: Ottakringer beer in a can, 16. district of Vienna
        - Bugl: The "end" of a bread
        - 
- Australian Hot Dog: https://www.yelp.at/biz/australian-hot-dog-wien ... when you are around late at night next to the party spots, this special hot dog is a pure joy.


## Shopping and Goods

Plan ahead with some luggage space to collect some Austrian goodies.

### Manner

There are several shops in Vienna, the most central one is at the Stephansplatz near the St. Stephen's Cathedral. You can get Manner wavers, Dragee Keksi, Chocolate bananas, and more.
https://josef.manner.com/en/shops

### Julius Meinl

Fine goods and (imho) the best coffee. There is a store in central Vienna, Am Graben.
https://www.meinlamgraben.at/Locations/Am-Graben-19-Wien 

## Relax

- [Danube Island](https://www.wien.info/en/vienna-for/families/outdoor/danube-island)


## Events

### Nighlife

- 1516, Irish pub: https://goo.gl/maps/feANvzPtjDBxWowE9
- 
